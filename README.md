# openplaylist

Open playlist is a software for playlist extraction and exporting in various
format, including extraction from websites 

Currently we only support extraction from Spotify and export to plain text,
csv or a folder of audio file from YouTube

It is *for research purposes only.*

## Requirements

`firefox+geckodriver` or `chromium` (`chromedriver` usually included), `python`, `python-selenium`, `python-pyacoustid`,
`python-musicbrainzngs` and `python-fuzzywuzzy`.

Additionally, for YouTube downloading, `youtube-dl` is required.

## Basic Usage

The main program is in `src/main.py`. For now only the most basic interaction is possible,
but for getting the list of titles of a Spotify playlist, do:

```
./src/main.py print https://open.spotify.com/playlist/your-playlist-id
./src/main.py print https://open.spotify.com/album/your-album-id
```

To get data in a csv file, you can do:

```
./src/main.py get https://open.spotify.com/playlist/your-playlist-id result.csv -o csv
```

To download a playlist from youtube, do

```
./src/main.py get https://open.spotify.com/playlist/your-playlist-id result_folder -o youtube
```

Possibly due to some bugs in python libraries, optional arguments must all be after positional arguments.

## Spotify extraction information

### Supported extraction methods

Version | Supported | Automated | Firefox support | Chromium support
------- | --------- | --------- | --------------- | ----------------
v3      | yes       | yes       | yes (86+)       | yes (87+)
v2      | yes       | no        | yes (84+)       | yes (87+)
v1      | no        | no        | /               | /

### v3 - automated extraction using Selenium

Both Firefox and Chromium support are available at the moment.
About recently added Firefox support, see FIREFOX.md.
Extraction v3 is the version used by the main program, see before.

### v2 - precise manual extraction using a bookmark

Bookmark `src/bookmarkv2.js` and extract precise information of each track of a Spotify playlist.
Extracted information includes for each track:
- the track id (position within the playlist),
- the track title,
- the track artist(s),
- the track album,
- the explicit tag if applicable,
- the date on which the track has been added to the playlist,
- the track duration.
The extracted information is output to the web console.

#### Using Firefox (tested on Firefox 84)

- Create a new bookmark, ideally in the bookmarks toolbar, and paste the content of `src/bookmarkv2.js` in the Location field.
- Open the Spotify playlist link.
- Open the Web Console (Ctrl+Shift+K or Menu > Web Developer > Web Console).
- Click on the created bookmark, wait for the end of the script's execution.
- In the Web Console, a log should normally appear, of the following form (`xxx` should be the number of tracks of the extracted playlist).
```javascript
[["1","Title 1",false,"Artist 1","Album 1","Date 1","x:xx"],["2","Title 2",true,"Artist 2","Album 2","Date 2","x:xx"],["3","Title 3",true,"Artist 3","Album 3","Date 3","x:xx"], ...]
```
- Select the whole line, and right-click on it, Copy Object.
- Paste the JSON-encoded data somewhere.

#### Using Chromium (tested on Chromium 87)

- Create a new bookmark, ideally in the bookmarks toolbar, and paste the content of `src/bookmarkv2.js` in the URL field.
- Open the Spotify playlist link.
- Open the Web Console (Ctrl+Shift+I > Console or Menu > More tools > Developer tools > Console).
- Click on the create bookmark, wait for the end of the script's execution.
- In the Web Console, a log should normally appear, of the following form (`xxx` should be the number of tracks of the extracted playlist).
```javascript
[["1","Title 1",false,"Artist 1","Album 1","Date 1","x:xx"],["2","Title 2",true,"Artist 2","Album 2","Date 2","x:xx"],["3","Title 3",true,"Artist 3","Album 3","Date 3","x:xx"], ...]
```
- At the end of this log, click on Copy.
- Paste the JSON-encoded data somewhere.


### v1 - proof-of-concept as bookmark

Bookmark `src/bookmarkv1.js` and extract rough information of each track of a Spotify playlist.


## Track identification

`openplaylist` also allow to identify tracks from simple audio files without metadata.
To test this feature, use `./src/main.py ident FILENAME`.
