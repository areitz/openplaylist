javascript:function sleep(n) { return new Promise(resolve => setTimeout(resolve, n)); }

/*
The main difficulty is that Spotify only keeps a "sliding window" of at most ~50 tracks
in the HTML tree whatever happens.
Thus, in order to recover all of the tracks of a playlist, one has to simulate the
scrolling, which is a bit obfuscated.
The used method to scroll here moves the sliding window by ~10 tracks at a time.
The two following global variables are used the following way, such that at each step:
- chunk_a represents the previous iteration of the extraction,
- chunk_b represents the current iteration of the extraction,
and before the following iteration, chunk_a and chunk_b are merged in chunk_a.
The merger is done using a common region approach.
*/
var chunk_a = "";
var chunk_b = "";

function getTracks() {
    /* Extract all text content as lines. */
    var all_text = document.getElementsByTagName("html")[0].textContent;
    var text_lines = all_text.trim().split("\n");

    /* It appears all of the relevant information is in the last non trivial chunk. */
    var k = 0;
    for (var i = 0; i < text_lines.length; i++) {
        if (text_lines[i].trim().length > 0) {
            k = i;
        }
    }

    /* Selected trimmed down brut chunk. */
    var text_line = text_lines[k].trim().split("duration")[1];

    /* Length of the common region and initialization. */
    var n = 20;
    var common;
    var common_pos;
    /* Initial configuration, chunk_a is empty. */
    if (chunk_a.length < n) {
        chunk_a = text_line;
    } else {
        /*
        - chunk_a = previous iteration
        - chunk_b = current iteration
        - chunk_a and chunk_b are merged in chunk_a at the end of the current iteration
        (common region approach)
        */
        chunk_b = text_line;
        common_region = chunk_b.slice(0, n);
        common_region_pos = chunk_a.indexOf(common_region);
        if (common_region_pos > -1) {
            chunk_b = chunk_a.slice(0, common_region_pos).concat(chunk_b);
            chunk_a = chunk_b;
            chunk_b = "";
        } else {
            alert("Error. No common region has been found.")
        }
    }
    /*alert(chunk_a.length);*/
    return chunk_a.length;
}
async function main() {
    /***
    Extraction
    ***/
    var old_size = 0;
    var new_size = 0;
    var i = 0;
    /*
    - old_size represents the size of the extracted chunk at the previous iteration
    - new_size represents the size of the extracted chunk at the current iteration,
    after the merger
    - both are compared until convergence
    */
    while (i === 0 || old_size < new_size) {
        old_size = new_size;
        new_size = getTracks();
        i += 1;

        /*
        Two divs are interesting about scrolling:
        - div[data-testid=top-sentinel],
        - div[data-testid=bottom-sentinel].
        Here, the second one is used in a natural way.
        */
        var e = document.querySelectorAll('div[data-testid=bottom-sentinel]');
        if (e.length > 0) {
            e[0].scrollIntoView();
        } else {
            alert("Error. Scrolling impossible.");
        }

        /*
        Time in milliseconds between each step, could be configured.
        */
        await sleep(500);
    }

    /* Regularly useful debug prints at this point. */
    /*alert(chunk_a.slice(0, 200));
    alert(chunk_a.slice(chunk_a.length - 200));
    alert(chunk_a);*/

    /***
    Parsing v2 - more precise, hard to separate title, artists and album
    the dates and durations are used as separators
    ***/
    var regexp_date = "((Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) [0-9]{1,2}, [0-9]{4})";
    var regexp_duration = "([0-9]{1,2}:[0-9]{2})";
    var regexp = new RegExp("("+regexp_date+regexp_duration+")");
    var tracks_data = chunk_a.split(regexp);
    alert(tracks_data.length);

    /* The 3 deterministically currently extracted informations. */
    var tracks = [];
    var dates_added = [];
    var durations = [];
    for (var i = 0; i < tracks_data.length; i++) {
        /*
        Splitting a string over a regex not only returns the substrings,
        but also all subexpressions of the regexp = regular expressions.
        Here,
        $0 = the substring of the chunk,
        $1 = all of the catched regexp,
        $2 = only the date,
        $3 = only the month,
        $4 = only the duration.
        */
        if (i % 5 === 0) {
            tracks[i] = tracks_data[i];
            dates_added[i] = tracks_data[i+2];
            durations[i] = tracks_data[i+4];
            alert(tracks[i]+" "+dates_added[i]+" "+durations[i]);
        }
    }

    /***
    Parsing v1 - very rough, OK for 20th century Spotify, without using regexp (code as bookmark)
    var t_approx_titles = chunk_a.split(" 20");
    alert(t_approx_titles.length);
    ***/

}
main();
