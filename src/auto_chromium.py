from selenium import webdriver
# console logging
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
# timeout handling
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
# headless
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.alert import Alert
# data organization + file handling
import os
import ast
import json
from track import Track

# for debugging purpose
from timeit import default_timer as timer
DEBUG = False
precision = 3
delay1 = 10
delay2 = 60
script_name = "bookmarkv2.js"

def json2tracklist(jsonstring):
    j = json.loads(jsonstring)
    l = []
    for jtrack in j:
        t = Track()
        t.title = jtrack["title"]
        t.artists = jtrack["artist"].split(", ")
        t.album = jtrack["album"]
        dur = jtrack["duration"].split(':')
        t.length = int(dur[0]) *60 + int(dur[1])
        l += [t]
    return l

def extract_playlist_data(url, headless):
    if DEBUG: timers = []


    # Logging should be very verbose, as the bookmark console log is not an error.
    desired_capabilities = DesiredCapabilities.CHROME
    desired_capabilities["goog:loggingPrefs"] = { "browser":"ALL" }
    # Headless Chromium is more comfortable to use and does not change the result.
    chromium_options = Options()
    if headless:
        chromium_options.add_argument("--headless")
    chromium_options.add_argument("--window-size=1366x768")
    # Just in case.
    chromium_options.add_argument("--incognito")


    if DEBUG: timers.append(timer())
    # Initialization of the browser.
    driver = webdriver.Chrome(
        options=chromium_options,
        desired_capabilities=desired_capabilities
    )
    driver.get(url)

    # Get the JavaScript script to be executed.
    with open("src/"+script_name, "r") as f:
        ext_js = f.read()[len("javascript:"):]

    # Parse the title.
    title = driver.title
    print(title)
    title = title.split("|")[0]
    title = title.replace(" ", "-")
    title = title.replace("'", "")
    title = title.lower()[:-1]


    # if DEBUG: timers.append(timer())
    # # Wait for the privacy popup, apparently redundant with the next wait.
    # try:
    #     complete_load_witness_elem = EC.presence_of_element_located(
    #         (By.ID, "onetrust-policy")
    #     )
    #     WebDriverWait(driver, delay).until(complete_load_witness_elem)
    # except TimeoutException:
    #     print("TimeoutException raised.")


    if DEBUG: timers.append(timer())
    # Wait for the tracks rows, JS script requirement.
    try:
        complete_load_witness_elem = EC.presence_of_element_located(
            (By.CSS_SELECTOR, "div[data-testid='tracklist-row']")
        )
        WebDriverWait(driver, delay1).until(complete_load_witness_elem)
    except TimeoutException:
        print("TimeoutException raised.")


    if DEBUG: timers.append(timer())
    # Alea jacta est.
    driver.execute_script(ext_js)


    if DEBUG: timers.append(timer())
    # Wait for the end of/an error from the JS script execution.
    try:
        complete_exec_witness = EC.alert_is_present()
        WebDriverWait(driver, delay2).until(complete_exec_witness)
    except TimeoutException:
        print("TimeoutException raised.")

    alert = Alert(driver)
    print("[", alert.text, "]")


    if DEBUG: timers.append(timer())
    # Parse the browser console log.
    log_found = False
    for log in driver.get_log("browser"):
        if log["level"] == "INFO" and log["source"] == "console-api":
            brut_data = log["message"]
            log_found = True
            break

    res = []
    if log_found:
        if DEBUG: print("OK, console log found.")
        data_as_json = brut_data.split(" ", 2)[2]
        #f.write(data_as_json)
        res = json2tracklist(ast.literal_eval(data_as_json))
    else:
        if DEBUG: print("Error, no console log found.")

    if DEBUG:
        print([round(timers[k+1]-timers[k], precision)
            for k in range(len(timers) - 1)])
    driver.quit()

    return res
