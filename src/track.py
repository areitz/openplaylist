

class Track:
    title = None
    artists = None
    album = None
    length = None # in s
    mbid = None

    def __init__(self):
        self.artists = []

    def __repr__(self):
        return str(self.title) + " - " + " & ".join(self.artists) + " | " + str(self.album)

    def __str__(self):
        return str(self.title) + " - " + " & ".join(self.artists) + " | " + str(self.album)
