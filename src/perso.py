from src.auto_chromium import extract_playlist_data

DIR = "data/"
DIR = "data3/"

import os
# 194 sons, SF
ca_zouke_pas_trop = "https://open.spotify.com/playlist/30o4eHkkRM9YEuAewXyCfy"
# 126 sons, SF
ca_zouke_un_peu = "https://open.spotify.com/playlist/6RkuZrzzNN9EjOyNcWY5Fh"
# 96 sons, SF
comment_ca_zouke = "https://open.spotify.com/playlist/3Ysm9cUzi1AOJy0riaBVtW"
# 115 sons, SF
on_est_chez_nous = "https://open.spotify.com/playlist/7zTpScqYl8wmBMLuu0Ltey"
# 342 sons, collab
dimanche_soir = "https://open.spotify.com/playlist/102mmnZj3ygZic1cBT3HhA"
# ?, AD
# 229
jazz_vibes = "https://open.spotify.com/playlist/7jKBzAiCfbKRqBjtnxrhpK"#?si=xQGr2GJnRpeL6aWhQYzkbw"
# 135
indie_vibes = "https://open.spotify.com/playlist/2Sn9uWuytFLcWvOcj3Ho62"#?si=ocGlgdMWTMOhA5CcdM1n7Q"
# 222
hit_the_road = "https://open.spotify.com/playlist/76T2n6GaefqC0Nougw5noK"#?si=FUeCi1D4RD6iy4NAMAQ-Gg"
# 110
classique = "https://open.spotify.com/playlist/3HbrnDLyCTtg3BsTS4yodJ"#?si=hwiBvvcoQXaO2Oec40XwQQ"

playlists = [
    ca_zouke_pas_trop,
    ca_zouke_un_peu,
    comment_ca_zouke,
    on_est_chez_nous,
    dimanche_soir
]
playlists2 = [
	jazz_vibes,
	indie_vibes,
	hit_the_road,
	classique
]

os.system("mkdir -p "+DIR)
for p in playlists2:
    extract_playlist_data(p, DIR)
