#! /usr/bin/env python

import sys
import os
import json
import argparse
import track
import auto_chromium
import auto_firefox
import ident
import csv
import track
import youtube

# This file define the main command line interface
parser = argparse.ArgumentParser(prog='openplaylist', description='Web playlist management')
parser.add_argument('--version', action='version', version='pre-alpha')
def defaultcmd(args):
    parser.print_usage()
    print("Use -h or --help for more help")

parser.set_defaults(cmd=defaultcmd)

sp = parser.add_subparsers(title="subcommands")

# Now is a list of all subcommands

# Get subcommand
# This subcommand is organised in a modular input-output way.
# The user can specify the input method among a list of 'extractors'
# On the CLI this extractor is specified with the -i/--input-type option
# A function with a matching key is then selected in the 'extractors' dictionary
# This function take the full args object and can use the value of any option
# to dedice what to do. In particular it is expected that it mainly uses
# args.source. It must return a list of tracks.
#

supported_browsers = {
    'firefox': auto_firefox.extract_playlist_data,
    'chromium': auto_chromium.extract_playlist_data,
}

headless_choices = {
    'y': True,
    'yes': True,
    'n': False,
    'no': False,
}

def spotifyextract(args):
    print("Browser: "+args.browser+".")
    return supported_browsers[args.browser](
        args.source,
        args.headless
    )
#
def nameextract(args):
    tmp = args.source.split("-")
    t = track.Track()
    t.title = tmp[0].strip()
    t.artists = [tmp[1].strip()]
    t.album = args.album
    return [t]


extractors = { 'spotify' : spotifyextract, 'name': nameextract}



# On the other side, the output method is called an exporter and
# specified using -o/--output-type. It must be in the 'exporters' map
# Functions in that map take the args object and the list of tracks.
# They should mainly use the args.dest value.

original_stdout = sys.stdout

#print a playlist on a stream
def printpl(pl, stream):
    sys.stdout = stream
    for i in range(len(pl)):
        print(i + 1,'. ', pl[i], sep='')
    sys.stdout = original_stdout

# Plain printing exporter
def plainprinter(args, pl):
    if args.dest == "":
        printpl(pl, sys.stdout)
    else:
        with open(args.dest, 'w') as f:
            printpl(pl,f)

# CSV exporter
def csvprinter(args, pl):
    if args.dest == "":
        print("CSV exporter needs a destination file")
        exit(1)
    else:
        with open(args.dest, 'w', newline='') as csvfile:
            csvwriter = csv.writer(csvfile, delimiter=',',
                            quotechar='"', quoting=csv.QUOTE_MINIMAL)
            for i in range(len(pl)):
                csvwriter.writerow([str(i + 1), pl[i].title,
                                    ' & '.join(pl[i].artists), pl[i].album])

def ytdowloader(args, pl):
    dest = args.dest if args.dest != "" else "out"
    os.mkdir(dest)
    for i in range(len(pl)):
        l = pl[i].length if pl[i].length != None else args.length
        youtube.download_track(pl[i], args.output_format,
                               dest+'/'+str(i+1).zfill(3) +'. ' + pl[i].title, args.num, l)

exporters = {'plain' : plainprinter, 'csv' : csvprinter, 'youtube' : ytdowloader }

# And the main getter subcommand function:

def gettercmd(args):
    res = extractors[args.input_type](args)
    exporters[args.output_type](args, res)

#And the getter subcommand setup
get_parser = sp.add_parser("get", help="Get a playlist and store it in the target")

get_parser.set_defaults(cmd=gettercmd)
get_parser.add_argument('-b', '--browser', default='firefox',
                        help="Browser to use for web extraction (default: firefox)")
get_parser.add_argument('--no-headless', action='store_false', dest='headless',
                        help="Show the browser window when extracting")
get_parser.add_argument('-i', '--input-type', default='spotify',
                        choices=extractors.keys(),
                        help="The extraction target type (default: spotify)")
get_parser.add_argument('source', help="The source of extraction, eg. a spotify link")
get_parser.add_argument('dest', help="The destination of extration eg. a folder")
get_parser.add_argument('-o', '--output-type', default='plain',
                        choices=exporters.keys(),
                        help="The type of export required (default: plain)")
get_parser.add_argument('-f', '--output-format', default='mp3',
                        help="The file format when exporting audiofiles (default: mp3)")
get_parser.add_argument('-a', '--album', default=None,
                        help="Prior information on album when unknown by the method of extraction")
get_parser.add_argument('-l', '--length', type=int, default=42069,
                        help="Prior information on length when unknown by the method of extraction")
get_parser.add_argument('-n', '--num', type=int, default=3,
                        help="Number of youtube videos to try before making a choice (default: 3)")


# The printer subcommand just call half of the pipeline:
# it calls and extractor and then print the result.

def printercmd(args):
    res = extractors[args.input_type](args)
    printpl(res, sys.stdout)

print_parser = sp.add_parser("print", help="Print a playlist")

print_parser.set_defaults(cmd=printercmd)
print_parser.add_argument('-b', '--browser', default='firefox',
                        help="Browser to use for web extraction (default: firefox)")
print_parser.add_argument('--no-headless', action='store_false', dest='headless',
                        help="Show the browser window when extracting")
print_parser.add_argument('-i', '--input-type', default='spotify',
                        choices=extractors.keys(),
                        help="The extraction target type (default: spotify)")
print_parser.add_argument('source', help="The source of extraction, eg. a spotify link")
print_parser.add_argument('-a', '--album', default=None,
                        help="Prior information on album when unknown by the method of extraction")

# The ident subcommand takes a file as input and tries to identify it.
# One may supply prior metadata to disambiguate

def identcmd(args):
    tr = ident.track_from_file(args.file, album=args.album, title=args.title, artists=args.artist)
    if(tr != None):
        print (tr)
        if (tr.mbid != None):
            print ("https://musicbrainz.org/recording/" + tr.mbid)
    else:
        print ("No matching track was found")

ident_parser = sp.add_parser("ident", help="Identifies a file with acoustID")

ident_parser.set_defaults(cmd=identcmd)
ident_parser.add_argument('file', help="The file to identify")
ident_parser.add_argument('-a', '--album', default=None,
                          help="Prior information on album")
ident_parser.add_argument('--artist', action='append',
                          help="Prior information on artist, may be specified multiple times")
ident_parser.add_argument('-t', '--title', default=None,
                          help="Prior information on title")


# Now we can actually run the command line

args = parser.parse_args()
args.cmd(args)
