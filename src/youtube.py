import shutil
import subprocess
import os
import ident
from fuzzywuzzy import fuzz
import time
import track

# Search the provided search string and return num youtube ids of matching videos
def search(search, num):
    assert (num > 0)
    while(True):
        res = subprocess.run(
            ["youtube-dl", "ytsearch" + str(num) + ':'+ search, "--get-id"],
            capture_output=True
        )
        if res.returncode != 0:
            print("Failed to do a youtube search for " + search + ": Retrying")
            time.sleep(1.0)
        else:
            return res.stdout.decode('utf-8').split('\n')[0:-1]

# Get the length of a youtube video through youtube-dl
def get_length(ytid):
    while(True):
        res = subprocess.run(["youtube-dl", "--get-duration", "--", ytid],
                             capture_output=True
                             )
        if res.returncode != 0:
            print("Failed to get length",ytid, "Retrying")
            time.sleep(1.0)
        else:
            l = res.stdout.decode('utf-8').strip().split(':')
            length = 0;
            for s in l:
                length *= 60
                length += int(s)
            return length

# Download a specific youtube video with the specified format into the specified file
def download(ytid, format, output_file):
    while(True):
        res = subprocess.run(
            ["youtube-dl", "-x", "--audio-format", format, "-o", output_file,
             "--", ytid,
            ]
        )
        if res.returncode != 0:
            print("Failed to download " + ytid, "Retrying")
            time.sleep(1.0)
        else:
            return


# Download the specified track with the specified format into the specified file
# It will try to download the first `totest` video that match the title and artist in
# youtube search.
# Then it tries to identify them with AcoustID and match that with the target name.
# And pick the one that matches best the input track
def download_track(tr, format, output_file, totest, expected_length):
    def tr_search_string(tr):
        return tr.title + " " + " ".join(tr.artists) + " audio"
    print ("[search] Searching", tr_search_string(tr))
    if totest == 1:
        id = search(tr_search_string(tr), 1)
        download(id[0], format, output_file)
        return

    possible_ids = search(tr_search_string(tr), totest)
    max_score = 0.0
    for pid in possible_ids:
        print("[id]", pid)
        g = get_length(pid)
        if g > 2 * expected_length:
            print(pid, "was too long for the expected length")
            continue
        download(pid, format, output_file + ".tmp." + format)
        backtr = ident.track_from_file(output_file + ".tmp." + format,
                                          tr.title, tr.artists, tr.album)
        print("[ident] identified as", backtr)
        if(backtr == None):
            print(pid, "was not identified and thus discarded")
            os.remove(output_file + ".tmp." + format)
            continue
        score = fuzz.token_set_ratio(tr.title, backtr.title)
        if tr.artists != None:
            score *= ident.list_fuzz(tr.artists, backtr.artists)
        if tr.album != None and backtr.album != None:
            score *= fuzz.token_set_ratio(tr.album, backtr.album) / 50
        print("[score]", pid, "got score", score)
        if score > max_score:
            os.replace(output_file + ".tmp." + format, output_file + '.' + format)
            max_score = score
            if score >= 19000:
                break
        else:
            os.remove(output_file + ".tmp." + format)
