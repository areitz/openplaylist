# This file is about music identification whether from metadata or just a plain audio file
import acoustid
import musicbrainzngs
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
import time
import track

# On acoustid, the API key is related to the application and not the user,
# that's why it's hardcoded here
acoustid_apikey="DZTWBDg1Ir"
# Musicbrainz requires us to access it's web API with a sensible useragent
musicbrainzngs.set_useragent("openplaylist", "0.0.1")


# Take a file name and give a list of potential matches represented
# as a pair of a score and a MBID
def get_mbid_from_file(path):
    res = []
    while (True):
        try:
            for score, recording_id, _, _ in acoustid.match(acoustid_apikey, path):
                res += [(score, recording_id)]
            return res
        except acoustid.WebServiceError as e:
            time.sleep(1.0)
            print("AcoustID web service error: Retrying (",e,")")

# Fetch track information from musicbrainz database using a MBID.
# If the recording was published into multiple albums, use album input to disambiguate
def track_from_mbid(mbid, album=None):
    try:
        result = musicbrainzngs.get_recording_by_id(mbid, includes=["artists", "releases"])
    except musicbrainzngs.WebServiceError:
        print("MBID " + mbid + " encountered an error")
        return None
    recording = result["recording"]
    tr = track.Track()
    tr.mbid = mbid
    tr.title = recording["title"]
    if "length" in recording:
        tr.length = float(recording["length"]) / 1000.0 # musicbrainz length is in ms
    for a in recording["artist-credit"]:
        if "artist" in a:
            tr.artists += [a["artist"]["name"]]

    albums = []
    for a in recording["release-list"]:
        albums += [a["title"]]
    if (albums == None or albums == []):
        tr.album = None
    elif(album == None):
        tr.album = albums[0]
    else:
        albums = list(set(albums))
        tr.album = process.extractOne(album, albums)[0]
    return tr

# Compute a score between two lists
def list_fuzz(l1, l2):
    l1.sort()
    l2.sort()
    s1 = " & ".join(l1)
    s2 = " & ".join(l2)
    return fuzz.token_sort_ratio(s1, s2)

# Try to disambiguate a list of mbids with some prior information
def track_from_mbids(mbids, title = None, artists = None, album=None):
    max_score = 0
    max_track = None
    for score, mbid in mbids:
        track = track_from_mbid(mbid,album)
        if (track == None):
            continue
        if(artists != None):
            score *= list_fuzz(artists,track.artists)
        if(title != None):
            score *= fuzz.ratio(title, track.title)
        if(album != None):
            score *= fuzz.ratio(album, track.album)
        if(score > max_score):
            max_score = score
            max_track = track
    return max_track

# Get track information from an audio file.
# Use optional arguments as prior information to disambiguate
def track_from_file(path, title = None, artists = None, album = None):
    return track_from_mbids(get_mbid_from_file(path), title, artists, album)
