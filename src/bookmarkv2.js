javascript:function sleep(n) { return new Promise(resolve => setTimeout(resolve, n)); }

/*
The main difficulty is that Spotify only keeps a "sliding window" of at most ~50 tracks
in the HTML tree whatever happens.
Thus, in order to recover all of the tracks of a playlist, one has to simulate the
scrolling, which is a bit obfuscated.
The used method to scroll here moves the sliding window by ~10 tracks at a time.
Tracks information are progressively parsed during this process.
*/

/*
The sliding window is taking the form of a table.
The structure of the relevant DOM elements is the following.
- header row
- a div containing the sliding window, whose id is obfuscated
-- a div whose id is obfuscated but with attribute data-testid="top-sentinel"
-- a div whose id is obfuscated containing all of the sliding window's currently loaded tracks
--- row containing track n_{1} information
--- ...
--- row containing track n_{2} information
-- a div whose id is obfuscated but with attribute data-testid="bottom-sentinel"

In the case of a playlist, the structure of each track row is the following.
- the track row div
-- the track row container (for the purpose of drag and drop),
containing a div for each column of the sliding window table
--- div 1 corresponding to the track id (position within the playlist)
--- div 2 corresponding to the track title, artist, song picture, explicit tag,
---- song picture
---- div 2 bis corresponding to the track title, artist, explicit tag
----- title
----- [explicit tag]
----- artist
--- div 3 corresponding to the track album
--- [div 3bis corresponding to the person who added the track to the playlist]
--- div 4 corresponding to the date on which the track was added to the playlist
--- div 5 corresponding to the track duration

In the case of an album, the structure of each track row is the following.
- the track row div
-- the track row container (for the purpose of drag and drop),
containing a div for each column of the sliding window table
--- div 1 corresponding to the track id (position within the playlist)
--- div 2 corresponding to the track title, artist, explicit tag,
---- div 2 bis corresponding to the track title, artist, explicit tag
----- title
----- [explicit tag]
----- artist
--- div 3 corresponding to the track duration
*/

/* tracks_data is the structure holding all of the so far collected information */
let tracks_data = new Map();

function getTracks(){
    /*** Parsing v3 - only the song picture is not taken into account as metadata ***/

    /* Assumption: the top sentinel is not obfuscated. */
    let e = document.querySelectorAll('div[data-testid=top-sentinel]');
    if (e.length < 1) {
        alert("Error. No top sentinel as DOM structure helper.");
        return -1;
    }
    let brut_page_title = document.title;
    let is_a_playlist = brut_page_title.includes(" - playlist");
    let is_an_album = brut_page_title.includes(" - Album");
    if (is_an_album) {
        page_title_album = brut_page_title.split(" - Album")[0];
    }
    if (!is_a_playlist && !is_an_album) {
        is_a_playlist = true;
        is_an_album = true;
        page_title_album = brut_page_title.split(" | ")[0];
    }

    let content = e[0].nextSibling;
    let rows = content.querySelectorAll('div[role=row]');
    for (let i = 0; i < rows.length; i++) {
        /* Assumption: only one child, the drag and drop container. */
        let dragndrop_div = rows[i].childNodes;
        if (dragndrop_div.length != 1) {
            alert("#1: "+i+" "+dragndrop_div.length);
            continue;
        }
        let columns = dragndrop_div[0].childNodes;
        /* Decision: five or six childs = playlist,
        for each column of the sliding window table. */
        if (is_a_playlist
            && (columns.length == 5 || columns.length == 6)) {
            /* Assumption: two childs,
            - one for the song picture,
            - one containing the track title, explicit tag and track artist. */
            let title_columns = columns[1].childNodes;
            if (title_columns.length != 2) {
                alert("#3: "+i+" "+title_columns.length);
            }
            /* Assumption: two or three childs,
            - one the for the title,
            - one for the eventual explicit tag,
            - one for the the artist. */
            let title_rows = title_columns[1].childNodes;
            if (title_rows.length != 2 && title_rows.length != 3) {
                alert("#4: "+i+" "+title_rows.length);
                continue;
            }

            let brut_id = columns[0];
            let brut_title = title_rows[0];
            let brut_explicit = title_rows.length == 3;
            let brut_artist = title_rows[title_rows.length - 1];
            let brut_album = columns[2];
            let brut_date_added = columns[columns.length - 2];
            let brut_duration = columns[columns.length - 1];

            let id = brut_id.textContent;
            let title = brut_title.textContent;
            let explicit = brut_explicit;
            let artist = brut_artist.textContent;
            let album = brut_album.textContent;
            let date_added = brut_date_added.textContent;
            let duration = brut_duration.textContent;

            let id_as_int = Number(id);
            let track_data = {
                id: id,
                title: title,
                explicit: explicit,
                artist: artist,
                album: album,
                date_added: date_added,
                duration: duration,
            };
            if (tracks_data.has(id_as_int)) {
                continue;
            } else {
                tracks_data.set(id_as_int, track_data);
            }
        /* Decision: three childs = album,
        for each column of the sliding window table. */
        } else if (is_an_album && columns.length == 3) {
            /* Assumption: one child,
            - one containing the track title, explicit tag and track artist. */
            let title_columns = columns[1].childNodes;
            if (title_columns.length != 1) {
                alert("#3: "+i+" "+title_columns.length);
            }
            /* Assumption: two or three childs,
            - one the for the title,
            - one for the eventual explicit tag,
            - one for the the artist. */
            let title_rows = title_columns[0].childNodes;
            if (title_rows.length != 2 && title_rows.length != 3) {
                alert("#4: "+i+" "+title_rows.length);
                continue;
            }

            let brut_id = columns[0];
            let brut_title = title_rows[0];
            let brut_explicit = title_rows.length == 3;
            let brut_artist = title_rows[title_rows.length - 1];
            let brut_duration = columns[columns.length - 1];

            let id = brut_id.textContent;
            let title = brut_title.textContent;
            let explicit = brut_explicit;
            let artist = brut_artist.textContent;
            let album = page_title_album;
            let duration = brut_duration.textContent;

            let id_as_int = Number(id);
            let track_data = {
                id: id,
                title: title,
                explicit: explicit,
                artist: artist,
                album: album,
                duration: duration,
            };
            if (tracks_data.has(id_as_int)) {
                continue;
            } else {
                tracks_data.set(id_as_int, track_data);
            }

        } else {
            alert("#2: "+i+" "+columns.length);
            continue;
        }
    }
    return tracks_data.size;
}

async function main(){
    /*** Extraction - progressive scrolling until convergence ***/
    let old_size = 0;
    let new_size = 0;
    let i = 0;
    /*
    - old_size is the size of the tracks_info structure at the previous iteration
    - new_size is the size of the tracks_info structure at the current iteration
    - i is the id associated with the current iteration
    */
    while (i == 0 || old_size < new_size) {
        old_size = new_size;
        new_size = getTracks();
        i += 1;

        /*
        Two divs are interesting about scrolling:
        - div[data-testid=top-sentinel],
        - div[data-testid=bottom-sentinel].
        Here, the second one is used in a natural way.
        Assumption: the bottom sentinel is not obfuscated.
        */
        let e = document.querySelectorAll('div[data-testid=bottom-sentinel]');
        if (e.length > 0) {
            e[0].scrollIntoView();
        } else {
            alert("Error. No bottom sentinel as scrolling helper.");
        }

        /* Time in milliseconds between each step, could be configured. */
        await sleep(500);
    }
    /* console.log(Array.from(tracks_data)); */
    console.log(JSON.stringify(Array.from(tracks_data.values())));
    alert(tracks_data.size);
}
main();
