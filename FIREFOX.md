# Firefox support and caveats

Everything can be done using Firefox almost the same way as when using Chromium, except the get_log function, which yields the following message.
```
selenium.common.exceptions.WebDriverException: Message: HTTP method not allowed
```

This is a known issue, with many many corresponding posts on GitHub, StackOverflow, etc.

```
https://github.com/mozilla/geckodriver/issues/284
https://github.com/mozilla/geckodriver/issues/284#issuecomment-477677764
https://github.com/SeleniumHQ/selenium/issues/1161
https://bugzilla.mozilla.org/show_bug.cgi?id=1453962
https://github.com/w3c/webdriver/issues/406
https://firefox-source-docs.mozilla.org/testing/geckodriver/TraceLogs.html
```

None of the encountered proposed workarounds (in Java or in Python) work.
The only working workaround I found using Python is to use a separate log file.

Firefox private browsing is not very explicit, unlike Chromium incognito mode.

```
https://stackoverflow.com/questions/27425116/python-start-firefox-with-selenium-in-private-mode
```
